# HTTPD

Nginx is being used to route HTTP requests to the appropriate services or applications based on the context of the request. This is typically done through configuration settings in Nginx, see **NGNIX** _configuration file_ according to the selected scenarios bellow.

Also it contains all docker-compose files to load the local environment scenarios for usage.

## Prerequisites

- Docker / Docker Compose
- Compiled HUB-REPO project
- Compiled HUB-SEARCH project
- Compiled HUB-STATISTICS project
- Compiled HUB-STATISTICS-UI project
- Compiled HUB-STORE project
- Compiled HUB-UI project

## Deploymet Scenarios

### Default

The default is including all the projects, setting up until now.

In the command line you can perform the following command

```shell
docker-compose -f docker-compose-local.yml up --build -d
```

The default includes the files:

1. docker-compose-local.yml
2. Dockerfile
3. ngnix.conf

### Scenario 1

This scenario is excluding the geovierwer projects.

In the command line you can perform the following command

```shell
docker-compose -f docker-compose-local-scen1.yml up --build -d
```

It includes the files:

1. docker-compose-local-scen1.yml
2. Dockerfile_scenario1
3. nginx_scenario1.conf

### Scenario 2

This scenario is excluding the geovierwer projects and mqa.

In the command line you can perform the following command

```shell
docker-compose -f docker-compose-local-scen2.yml up --build -d
```

It includes the files:

1. docker-compose-local-scen2.yml
2. Dockerfile_scenario2
3. nginx_scenario2.conf

## Access HealthData portal

[http://healthdata-local.com/](http://healthdata-local.com/)

