#Hub Statistics or piveau-hub-statistics
=====================
The Hub Statistics sub-project within the Data Europa portal serves as a crucial data collection and analysis engine, gathering and processing statistics from across the portal. These statistics provide valuable insights the overall effectiveness of the portal in facilitating data discovery and utilization.

Key Functions of Hub Statistics:

- Datasets by Data Theme: Hub Statistics provides insights into the distribution of datasets across various data themes, highlighting the breadth and diversity of data available within the portal.

- Datasets per Origin: Hub Statistics tracks the origin of datasets, identifying the contributing data providers and organizations. This information can inform collaboration efforts and strengthen the open data ecosystem.

- Datasets per Origin and Catalogue: Hub Statistics provides a detailed breakdown of datasets by both origin and catalogue, demonstrating the distribution of data across different thematic areas and data repositories.

- Total Number of Datasets: Hub Statistics tracks the overall number of datasets within the portal, providing a snapshot of the portal's growth and the volume of data available for discovery and utilization.

- Datasets by Data Theme over Time: Hub Statistics analyzes the evolution of dataset distribution across data themes over time, identifying emerging trends and potential shifts in data focus.

- Datasets per Origin over Time: Hub Statistics tracks the origin of datasets over time, demonstrating how the portal's data source diversity has evolved.

- Datasets per Origin and Catalogue over Time: Hub Statistics provides a comprehensive analysis of dataset distribution across origin, catalogue, and time, offering a nuanced understanding of the portal's data landscape and usage patterns.


There are two ways to start this application. 

**1. Using Docker**
NOTE: we fully recommend to use the docker compose file available into the project httpd.

Note: using docker the developer don't need to compile anything first, docker will be doing it, while building the docker container.

First step, build the docker image.

    cd <path_to>/hub-statistics
    
    docker build -t hub-statistics .
    
Second step, run this image.

    docker run -p 9090:9090 --name statistics -d hub-statistics
    

**2. As a Python Application**

To start the service as a Python application, a **Python 3.5 or higher** installation is required. 
It is recommended to use *virtualenv* ([User Guide](https://virtualenv.pypa.io/en/latest/userguide/)). 
You can download it via *PyPi* ([Installation Guide](https://virtualenv.pypa.io/en/latest/installation/)):

    pip install virtualenv
    
Change in the programm directory.

    cd hub-statistics
    
Run virtualenv with the name `env`.

    virtualenv env

Activate the virtual environment.

    source env/bin/activate
    
Now use the `requirements.txt` to install all necessary dependencies via _PyPi_.
The `*` stands for `dev` or `prod`.

    pip install -r requirements_*.txt
    
After that, leave the virtual environment.

    deactivate
    
You can now use the script `start_server.sh` to start a local server, 
as it will use exactly this environment `env`. Use a parameter named `DEV` 
to start this server in _Debug-Mode_. 

    sh start_server.sh DEV 

Dependencies can also be found in the `./requirements_dev.txt` file.
These can be downloaded and installed using the following command: 

`pip install <dependencyname>==<version>`

necessary Dependencies :

| Dependency | Version |
| :--------- | :------ |
| Flask      | 1.0.2   |
| openpyxl   | 2.5.3   |
| celery     | 4.2.0   |
| requests   | 2.19.1  |
| schedule   | 0.5.0   |
| SQLAlchemy | 1.3.6   |
| pandas     | 0.25.1  |
| psycopg2-binary | 2.8.3|


