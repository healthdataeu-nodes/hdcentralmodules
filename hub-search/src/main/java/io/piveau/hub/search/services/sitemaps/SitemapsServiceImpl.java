package io.piveau.hub.search.services.sitemaps;

import io.piveau.hub.search.util.index.IndexManager;
import io.piveau.hub.search.util.request.Query;
import io.piveau.hub.search.util.response.ReturnHelper;
import io.piveau.hub.search.util.search.SearchClient;
import io.piveau.hub.search.util.sitemap.Sitemap;
import io.piveau.hub.search.util.sitemap.SitemapIndex;
import io.vertx.core.*;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.file.FileSystemException;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.serviceproxy.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.StringWriter;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.NoSuchFileException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class SitemapsServiceImpl implements SitemapsService {

    private final SearchClient searchClient;

    // path for sitemap xml
    private String writepath;
    private String readpath;

    // sitemap config
    private final JsonObject sitemapConfig;

    // vertx
    private final Vertx vertx;

    // executer
    private final WorkerExecutor executor;

    private static final Logger LOG = LoggerFactory.getLogger(SitemapsService.class);

    SitemapsServiceImpl(Vertx vertx, JsonObject esConfig, IndexManager indexManager, JsonObject sitemapConfig,
                        Handler<AsyncResult<SitemapsService>> handler) {
        this.searchClient = SearchClient.build(vertx, esConfig, indexManager);

        this.sitemapConfig = sitemapConfig;
        this.vertx = vertx;

        this.executor = vertx.createSharedWorkerExecutor("sitemaps-worker-pool", 3, 3, TimeUnit.MINUTES);

        Promise<Void> createFolder1 = Promise.promise();
        Promise<Void> createFolder2 = Promise.promise();

        vertx.fileSystem().mkdir("./sitemaps", createSitemapFolder -> {
            if (createSitemapFolder.succeeded() ||
                    createSitemapFolder.cause().getCause() instanceof FileAlreadyExistsException) {
                vertx.fileSystem().mkdir("./sitemaps/1", createSitemap1Folder -> {
                    if (createSitemap1Folder.succeeded() ||
                            createSitemap1Folder.cause().getCause() instanceof FileAlreadyExistsException) {
                        createFolder1.complete();
                    } else {
                        LOG.error("Sitemap: {}", createSitemap1Folder.cause().getMessage());
                        createFolder1.fail(createSitemap1Folder.cause());
                    }
                });

                vertx.fileSystem().mkdir("./sitemaps/2", createSitemap2Folder -> {
                    if (createSitemap2Folder.succeeded() ||
                            createSitemap2Folder.cause().getCause() instanceof FileAlreadyExistsException) {
                        createFolder2.complete();
                    } else {
                        LOG.error("Sitemap: {}", createSitemap2Folder.cause().getMessage());
                        createFolder2.fail(createSitemap2Folder.cause());
                    }
                });
            } else {
                LOG.error("Sitemap: {}", createSitemapFolder.cause().getMessage());
                createFolder1.fail(createSitemapFolder.cause());
                createFolder2.fail(createSitemapFolder.cause());
            }
        });

        CompositeFuture.all(createFolder1.future(), createFolder2.future()).onComplete(createFoldersResult -> {
            if (createFoldersResult.succeeded()
                    || createFoldersResult.cause() instanceof FileAlreadyExistsException) {
                Promise<Long> sitemap1 = Promise.promise();
                Promise<Long> sitemap2 = Promise.promise();

                vertx.fileSystem().props("./sitemaps/1/sitemap_index.xml", ar -> {
                    if (ar.succeeded()) {
                        sitemap1.complete(ar.result().lastModifiedTime());
                    } else {
                        sitemap1.fail(ar.cause());
                    }
                });

                vertx.fileSystem().props("./sitemaps/2/sitemap_index.xml", ar -> {
                    if (ar.succeeded()) {
                        sitemap2.complete(ar.result().lastModifiedTime());
                    } else {
                        sitemap2.fail(ar.cause());
                    }
                });

                CompositeFuture.all(sitemap1.future(), sitemap2.future()).onComplete(ar -> {
                    if (sitemap1.future().succeeded() && sitemap2.future().succeeded()) {
                        if (sitemap1.future().result() <= sitemap2.future().result()) {
                            writepath = "./sitemaps/1/";
                            readpath = "./sitemaps/2/";
                        } else {
                            writepath = "./sitemaps/2/";
                            readpath = "./sitemaps/1/";
                        }
                    } else if (sitemap1.future().succeeded()) {
                        writepath = "./sitemaps/2/";
                        readpath = "./sitemaps/1/";
                    } else if (sitemap2.future().succeeded()) {
                        writepath = "./sitemaps/1/";
                        readpath = "./sitemaps/2/";
                    } else {
                        writepath = "./sitemaps/1/";
                        readpath = "./sitemaps/2/";
                    }
                    // start periodic sitemap generation
                    vertx.setPeriodic(sitemapConfig.getInteger("interval", 86400000), periodic ->
                            executor.executeBlocking(promise -> {
                                generateSitemaps();
                                promise.complete();
                            }, res -> {
                            })
                    );
                });
                handler.handle(Future.succeededFuture(this));
            } else {
                handler.handle(Future.failedFuture(createFoldersResult.cause()));
            }
        });
    }

    @Override
    public Future<JsonObject> readSitemapIndex() {
        Promise<JsonObject> promise = Promise.promise();
        vertx.fileSystem().readFile(readpath + "sitemap_index.xml", ar -> {
            if (ar.succeeded()) {
                promise.complete(ReturnHelper.returnSuccess(200, ar.result().toString()));
            } else {
                if (ar.cause() instanceof FileSystemException && ar.cause().getCause() instanceof NoSuchFileException) {
                    promise.fail(new ServiceException(404, "Sitemap index not found"));
                } else {
                    promise.fail(new ServiceException(500, ar.cause().getMessage()));
                }
            }
        });
        return promise.future();
    }

    @Override
    public Future<JsonObject> readSitemap(String sitemapId) {
        Promise<JsonObject> promise = Promise.promise();
        vertx.fileSystem().readFile(readpath + "sitemap_datasets_" + sitemapId + ".xml", ar -> {
            if (ar.succeeded()) {
                promise.complete(ReturnHelper.returnSuccess(200, ar.result().toString()));
            } else {
                if (ar.cause() instanceof FileSystemException && ar.cause().getCause() instanceof NoSuchFileException) {
                    promise.fail(new ServiceException(404, "Sitemap not found"));
                } else {
                    promise.fail(new ServiceException(500, ar.cause().getMessage()));
                }
            }
        });
        return promise.future();
    }

    @Override
    public Future<JsonObject> triggerSitemapGeneration() {
        Promise<JsonObject> promise = Promise.promise();
        executor.executeBlocking(executeBlockingPromise -> {
            generateSitemaps();
            executeBlockingPromise.complete();
        }, res -> {
        });
        promise.complete(ReturnHelper.returnSuccess(200, "Triggered sitemap generation"));
        return promise.future();
    }

    private void generateSitemaps() {
        int size = sitemapConfig.getInteger("size", 10000);
        scrollIds(size).onSuccess(result -> {
            List<Future> writeFilePromises = new ArrayList<>();

            int amount = result.size();
            String sitemapIndex = generateSitemapIndex(amount);
            Promise writeSitemapIndex = Promise.promise();
            writeFilePromises.add(writeSitemapIndex.future());
            if (sitemapIndex != null && !sitemapIndex.isEmpty()) {
                vertx.fileSystem().writeFile(writepath + "sitemap_index.xml",
                        Buffer.buffer(sitemapIndex.getBytes()), writeFileResult -> {
                            if (writeFileResult.succeeded()) {
                                writeSitemapIndex.complete();
                            } else {
                                LOG.error("Sitemap: {}", writeFileResult.cause().getMessage());
                                writeSitemapIndex.fail(writeFileResult.cause());
                            }
                        });
            } else {
                writeSitemapIndex.fail(new NullPointerException(sitemapIndex));
            }

            for (int i = 0; i < amount; ++i) {
                String sitemap = generateSitemap(result.getJsonArray(i));
                Promise writeSitemap = Promise.promise();
                writeFilePromises.add(writeSitemap.future());
                if (sitemap != null && !sitemap.isEmpty()) {
                    vertx.fileSystem().writeFile(writepath + "sitemap_datasets_" + (i + 1) + ".xml",
                            Buffer.buffer(sitemap.getBytes()), writeFileResult -> {
                                if (writeFileResult.succeeded()) {
                                    writeSitemap.complete();
                                } else {
                                    LOG.error("Sitemap: {}", writeFileResult.cause().getMessage());
                                    writeSitemap.fail(writeFileResult.cause());
                                }
                            });
                } else {
                    writeSitemap.fail(new NullPointerException(sitemap));
                }
            }

            CompositeFuture.all(writeFilePromises).onComplete(
                    writeFilesResult -> {
                        readpath = writepath;
                        writepath = writepath.equals("./sitemaps/1/") ? "./sitemaps/2/" : "./sitemaps/1/";
                    });
        }).onFailure(failure -> {
            LOG.error("Sitemap: {}", failure.getMessage());
        });
    }

    private String generateSitemapIndex(int count) {

        SitemapIndex sitemapIndex = new SitemapIndex();

        sitemapIndex.addSitemap(sitemapConfig.getString("drupal", ""));

        for (int i = 0; i < count; ++i) {
            sitemapIndex.addSitemap(sitemapConfig.getString("url", "")
                    + "sitemap_datasets_" + (i + 1) + ".xml");
        }

        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(SitemapIndex.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

            // output pretty printed
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            StringWriter sw = new StringWriter();
            jaxbMarshaller.marshal(sitemapIndex, sw);
            // DEBUG: jaxbMarshaller.marshal(feed, System.out);

            return sw.toString();
        } catch (JAXBException e) {
            LOG.error("Sitemap: {}", e.getMessage());
            return null;
        }
    }

    private String generateSitemap(JsonArray entries) {

        Sitemap sitemap = new Sitemap();

        for (Object obj : entries) {
            JsonObject entry = (JsonObject) obj;

            String language = "en";

            sitemap.addSitemap(
                    sitemapConfig.getString("url", "") + "datasets" + "/" + entry.getString("id"),
                    entry.getJsonObject("catalog_record", new JsonObject()).getString("modified"),
                    language,
                    sitemapConfig.getJsonArray("languages", new JsonArray())
            );
        }

        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(Sitemap.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

            // output pretty printed
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            StringWriter sw = new StringWriter();
            jaxbMarshaller.marshal(sitemap, sw);
            // DEBUG: jaxbMarshaller.marshal(feed, System.out);

            return sw.toString();
        } catch (JAXBException e) {
            LOG.error("Sitemap: {}", e.getMessage());
            return null;
        }
    }

    private Future<JsonArray> scrollIds(Integer size) {
        Promise<JsonArray> promise = Promise.promise();

        JsonObject q = new JsonObject();
        q.put("size", size);
        q.put("filter", "dataset");
        q.put("aggregation", false);
        q.put("includes", new JsonArray().add("id").add("catalog_record.modified"));
        q.put("scroll", true);

        Query query = Json.decodeValue(q.toString(), Query.class);

        searchClient.listIds(query, true, false).onSuccess(promise::complete).onFailure(promise::fail);

        return promise.future();
    }

}
