# Hub Statistics UI

The Hub Statistics UI within the Health Data@EU portal acts as the user-facing interface for visualizing and analyzing catalogue and/or metadata records statistics collected by the Hub Statistics sub-project. 

Key Features of the Hub Statistics UI:

- The Hub Statistics UI provides detailed statistics for Current state, like:
    - Datasets by data theme
    - Datasets per origin
    - Datasets per origin and catalogue

- and provides detailed statistics about "Evolution over time" like:
    - Total Number of Datasets
    - Datasets by data theme
    - Datasets per origin
    - Datasets per origin and catalogue


## Setup

Clone the repository:

```
git clone git clone git@code.europa.eu:healthdataeu-nodes/hdcentralmodules.git
```

Navigate to the new directory:

```
cd hub-statistics-ui
```

Install all needed packages: 

```
npm install
```

### Using Docker 

Build, it will create the dist directory

```
$ npm run build 
```
Run the following script, it will create  dist.tar.gz, used by docker.

```
./createDist-tar.sh
```

Now it's all prepared to use the docker compose file into the project httpd

### Working without docker
Note: be sure that the port 8080 will be using on your local machine, typically it will be used by hub-ui.

Run the development server:

```
npm run dev
```

After this, the application will be running on http://localhost:8080.