const config = {
  title: 'SHACL Validator - data.europa.eu',
  description: 'SHACL Validator - data.europa.eu',
  link: 'https://www.data.europa.eu',
};

export { config };
