/* eslint-disable */
import Vue from 'vue';
import VueMeta from 'vue-meta';
import Router from 'vue-router';
import Main from '@/components/Main';
import EditorTab from "@/components/EditorTab";
import UrlTab from "@/components/UrlTab";

Vue.use(VueMeta);
Vue.use(Router);

const router = new Router({
  base: '/mqa/shacl-validator-ui',
  mode: 'history',
  linkActiveClass: 'active',
  routes: [
    {
      path: '/',
      redirect: '/data-provision'
    },
    {
      path: '/data-provision',
      name: 'DataProvision',
      component: UrlTab
    },
    {
      path: '/editor',
      name: 'Editor',
      component: EditorTab
    }
  ]
});

router.beforeEach((to, from, next) => {
  next();
});

export default router;
