# HUB-REPO

The hub-repo is the core module of the Piveau Data Platform, a robust data management solution tailored for large-scale open data. This module serves as the backbone, orchestrating the synchronization between the triple store and the search index, while also providing a feature-rich RESTful API. Developed in Java, it empowers users to create catalogs and metadata records seamlessly.

##Project Overview
In the context of the hub-repo sub-project, two pivotal components shape the data management infrastructure:

1. Piveau Data Platform
The Piveau Data Platform stands as a comprehensive solution designed explicitly for handling extensive open data. It leverages semantic web technologies to provide a scalable, flexible, and interoperable framework for managing, publishing, and accessing open data.

Key Features:
- Semantic Data Management: Utilizes RDF (Resource Description Framework) and OWL (Web Ontology Language) to represent data in a structured and meaningful manner, enabling efficient knowledge discovery and reasoning.

- Scalability and Performance: Engineered to handle large-scale open data volumes with high performance and scalability, ensuring efficient data storage, retrieval, and processing.

- Interoperability and Open Standards: Adheres to open standards and protocols, facilitating seamless data exchange and integration with other platforms and applications.

2. Triple Store
The Triple Store employed in the hub-repo sub-project, powered by Virtuoso, is a specialized database crafted for storing and managing RDF data. It adeptly organizes and indexes RDF triples, the fundamental building blocks of semantic data, facilitating rapid retrieval and querying of complex relationships and knowledge patterns.

Role and Functionality:
The triple store within hub-repo plays a pivotal role in managing the semantic representation of data. It stores and maintains RDF triples, representing various entities, relationships, and concepts within the data. This architecture enables efficient querying and reasoning using SPARQL, the standard query language for RDF data.

## Required Services

Required Piveau Services
In order to run the hub requires some other services

- Virtuoso Triplestore (Mandatory)
- piveau-search (Mandatory)
- piveau-shacl-validation (Recommended by original project but not yet integrated in HD@EU)
- piveau-data-upload (Recommended, hub-store)
- piveau-translation-service (Optional, it will be necessary. Not yet integrated  in HD@EU)
  - one of the options available is:
    - eTranslation
    - https://github.com/piveau-data/piveau-hub-translation
- Keycloak (Optional) not yet integrated.
- Virtuoso Shadow Triplestore (Optional)
    - The shadow triplestore stores hidden content that must not be accessible through virtuoso's public API
      - This includes:
        - Dataset drafts
        - Metrics history

## Prerequisites

- Java JDK 17
- Maven 3

## Setup

1 - Clone the hub-repo repository 
2 - Clone the hub-search repository (configure it, see the README.md file)
3 - Clone the hub-store repository (recommended, configure it, see the README.md file)

NOTE: the project already has default config file: conf/config.sample.json.

4 - build:

```bash
$ mvn clean install -DskipTests
$ docker-compose up -d
```
- Wait a couple of seconds.
    - you can check if the containers are up and running by applying the following commands to see the logs:
    
    ```bash    
    # hub-repo logs
    $ docker logs -f piveau-hub-repo
    
    #hub-search 
    $ docker logs -f piveau-hub-search
    
    #hub-store
    $ docker logs -f piveau-hub-simple-store
    
    #Virtuoso
    $ docker logs -f virtuoso    
    ```
- Check if the search service initialised properly by browsing to http://localhost:9080

## Configuration

- A sample configuration can be found in conf/config.sample.json
- The sample configuration works well with the provided docker-compose file

| Name                                                                       | Description                                                       | Type                  |
|:---|:---|:---|
| `PIVEAU_HUB_SERVICE_PORT`                                                    | The port for the service                                          | number                |
| `PIVEAU_HUB_API_KEY`                                                         | The API key of the service                                        | string                |
| `PIVEAU_HUB_API_KEYS`                                                        | A map of API keys associated with a list of resources             | json                  |
| `PIVEAU_HUB_AUTHORIZATION_PROCESS_DATA.clientId `                            | Client ID of backend instance                                     | string                |
| `PIVEAU_HUB_AUTHORIZATION_PROCESS_DATA.clientSecret`                         | Client secret of backend instance                                 | string                |
| `PIVEAU_HUB_AUTHORIZATION_PROCESS_DATA.tokenServerConfig`                    | Token server config                                               | json                  |
| `PIVEAU_HUB_AUTHORIZATION_PROCESS_DATA.tokenServerConfig.keycloak`           | Keycloak config                                                   | json                  |
| `PIVEAU_HUB_AUTHORIZATION_PROCESS_DATA.tokenServerConfig.keycloak.realm`     | Keycloak realm name                                               | string                |
| `PIVEAU_HUB_AUTHORIZATION_PROCESS_DATA.tokenServerConfig.keycloak.serverUrl` | Keycloak Host                                                     | string                |
| `PIVEAU_HUB_BASE_URI`                                                        | The RDF base url                                                  | string                |
| `PIVEAU_HUB_FORCE_UPDATES`                                                   | Forcing update on put dataset                                     | boolean               |
| `PIVEAU_TRIPLESTORE_CONFIG.address`                                          | URL of the triplestore                                            | string                |
| `PIVEAU_TRIPLESTORE_CONFIG.data_endpoint`                                    | Relative CRUD endpoint of the triplestore                         | string                |
| `PIVEAU_TRIPLESTORE_CONFIG.query_endpoint`                                   | Relative query endpoint of the triplestore                        | string                |
| `PIVEAU_TRIPLESTORE_CONFIG.username`                                         | Username for the triplestore                                      | string                |
| `PIVEAU_TRIPLESTORE_CONFIG.password`                                         | Password for the triplestore                                      | string                |
| `PIVEAU_SHADOW_TRIPLESTORE_CONFIG.address `                                  | URL of the shadow triplestore                                     | string                |
| `PIVEAU_SHADOW_TRIPLESTORE_CONFIG.username`                                  | Username for the shadow triplestore                               | string                |
| `PIVEAU_SHADOW_TRIPLESTORE_CONFIG.password`                                  | Password for the shadow triplestore                               | string                |
| `PIVEAU_HUB_VALIDATOR.enabled `                                              | Enable the use of the validator                                   | bool                  |
| `PIVEAU_HUB_VALIDATOR.history`                                               | Enable metrics history                                            | bool                  |
| `PIVEAU_HUB_VALIDATOR.metricsPipeName`                                       | Name of the validation pipe                                       | string                |
| `PIVEAU_HUB_SEARCH_SERVICE.enabled`                                          | Enable the use of the indexing                                    | string                |
| `PIVEAU_HUB_SEARCH_SERVICE.url`                                              | Host of the piveau-search service                                 | string                |
| `PIVEAU_HUB_SEARCH_SERVICE.port`                                             | Port of the piveau-search service                                 | number                |
| `PIVEAU_HUB_SEARCH_SERVICE.api_key`                                          | API key of the piveau-search service                              | string                |
| `PIVEAU_HUB_LOAD_VOCABULARIES`                                               | Enable the loading of RDF vocabularies                            | bool                  |
| `PIVEAU_HUB_LOAD_VOCABULARIES_FETCH `                                        | Enable the loading of RDF vocabularies from remote                | bool                  |
| `PIVEAU_TRANSLATION_SERVICE.enable`                                          | Enable the machine translation service                            | bool                  |
| `PIVEAU_TRANSLATION_SERVICE.accepted_languages `                             | Target languages to be translated                                 | array                 |
| `PIVEAU_TRANSLATION_SERVICE.translation_service_url `                        | URL of the translation service                                    | string                |
| `PIVEAU_TRANSLATION_SERVICE.callback_url `                                   | URL of the callback for the translation service                   | string                |
| `PIVEAU_DATA_UPLOAD.url`                                                     | URL of the data upload service                                    | string                |
| `PIVEAU_DATA_UPLOAD.service_url`                                             | Base URL of the download URL for the data                         | string                |
| `PIVEAU_DATA_UPLOAD.api_key`                                                 | API key of the data upload service                                | string                |
| `PIVEAU_HUB_CORS_DOMAINS`                                                    | Remote URLs, without protocol, that are allowed to access the hub | JSON Array of strings |
| `PIVEAU_HUB_CORS_DOMAINS`                                                    | Remote URLs, without protocol, that are allowed to access the hub | JSON Array of strings |
| `PIVEAU_FAVICON_PATH `                                                       | Path to a favicon. Can be a web resource                          | string or URL         |
| `PIVEAU_LOGO_PATH `                                                          | Path to a logo. Can be a web resource                             | string or URL         |
| `PIVEAU_IMPRINT_URL`                                                        | URL to imprint page. Used for OpenAPI GDPR complience             | URL                   |
| `PIVEAU_PRIVACY_URL`                                                       | URL to privacy policy page. Used for OpenAPI GDPR complience      | URL                   |
| `greeting`                                                                  | Meaningless string                                                | string                |

## Known Issues

### Elasticsearch

- It may be possible that Elasticsearch won't start, which requires some tweaks. Please refer to this documentation: https://gitlab.fokus.fraunhofer.de/viaduct/viaduct-hub-search/wikis/how-to-deploy-elasticsearch

# HUB-REPO OpenAPI

In the actual docker network, all available API's can be seen though this link: [http://healthdata-local.com/hub/api/repo/](http://healthdata-local.com/hub/api/repo/) (available only in the local machine)

For testing purposes, it's only being used the following API's:

<table>
<colgroup>
<col>
<col>
<col>
<col>
<col>
<col></colgroup>
<thead>
<tr>
<th data-column="0" tabindex="0" scope="col" role="columnheader" aria-disabled="false" unselectable="on" aria-sort="none" aria-label="Purpose: No sort applied, activate to apply an ascending sort" style="user-select: none;">
<div class="tablesorter-header-inner">Purpose</div>
</th>
<th class="confluenceTh tablesorter-header sortableHeader tablesorter-headerUnSorted" data-column="1" tabindex="0" scope="col" role="columnheader" aria-disabled="false" unselectable="on" aria-sort="none" aria-label="Operation: No sort applied, activate to apply an ascending sort" style="user-select: none;">
<div class="tablesorter-header-inner">Operation</div>
</th>
<th class="confluenceTh tablesorter-header sortableHeader tablesorter-headerUnSorted" data-column="2" tabindex="0" scope="col" role="columnheader" aria-disabled="false" unselectable="on" aria-sort="none" aria-label="Header: No sort applied, activate to apply an ascending sort" style="user-select: none;">
<div class="tablesorter-header-inner">Header</div>
</th>
<th class="confluenceTh tablesorter-header sortableHeader tablesorter-headerUnSorted" data-column="3" tabindex="0" scope="col" role="columnheader" aria-disabled="false" unselectable="on" aria-sort="none" aria-label="Body: No sort applied, activate to apply an ascending sort" style="user-select: none;">
<div class="tablesorter-header-inner">Body</div>
</th>
<th class="confluenceTh tablesorter-header sortableHeader tablesorter-headerUnSorted" data-column="4" tabindex="0" scope="col" role="columnheader" aria-disabled="false" unselectable="on" aria-sort="none" aria-label="Link: No sort applied, activate to apply an ascending sort" style="user-select: none;">
<div class="tablesorter-header-inner">Link</div>
</th>
<th class="confluenceTh tablesorter-header sortableHeader tablesorter-headerUnSorted" data-column="5" tabindex="0" scope="col" role="columnheader" aria-disabled="false" unselectable="on" aria-sort="none" aria-label="return: No sort applied, activate to apply an ascending sort" style="user-select: none;">
<div class="tablesorter-header-inner">return</div>
</th>
</tr>
</thead>
<tbody aria-live="polite" aria-relevant="all">
<tr role="row">
<td class="confluenceTd">List all catalogues</td>
<td class="confluenceTd">GET</td>
<td class="confluenceTd">
<p>Nothing to set&nbsp;</p>
</td>
<td class="confluenceTd">Nothing to set</td>
<td class="confluenceTd">&nbsp;
<span style="color: rgb(33,33,33);">
<a href="http://healthdata-local.com/hub/api/repo/catalogues" class="external-link" rel="nofollow">http://healthdata-local.com/hub/api/repo/catalogues</a>
</span>&nbsp;
</td>
<td class="confluenceTd">A list of catalogues</td>
</tr>
<tr role="row">
<td class="confluenceTd">Create a ecdc catalogue entity</td>
<td class="confluenceTd">PUT</td>
<td class="confluenceTd">
<div class="table-wrap">
<table data-mce-resize="false" class="confluenceTable tablesorter tablesorter-default" role="grid" resolved="">
<colgroup>
<col>
<col></colgroup>
<thead>
<tr role="row" class="tablesorter-headerRow">
<th class="confluenceTh tablesorter-header sortableHeader tablesorter-headerUnSorted" data-column="0" tabindex="0" scope="col" role="columnheader" aria-disabled="false" unselectable="on" aria-sort="none" aria-label="key: No sort applied, activate to apply an ascending sort" style="user-select: none;">
<div class="tablesorter-header-inner">key</div>
</th>
<th class="confluenceTh tablesorter-header sortableHeader tablesorter-headerUnSorted" data-column="1" tabindex="0" scope="col" role="columnheader" aria-disabled="false" unselectable="on" aria-sort="none" aria-label="value: No sort applied, activate to apply an ascending sort" style="user-select: none;">
<div class="tablesorter-header-inner">value</div>
</th>
</tr>
</thead>
<tbody aria-live="polite" aria-relevant="all">
<tr role="row">
<td class="confluenceTd">&nbsp;
<span style="color: rgb(33,33,33);">Content-Type</span>&nbsp;
</td>
<td class="confluenceTd">&nbsp;
<span style="color: rgb(33,33,33);">application/rdf+xml</span>&nbsp;
</td>
</tr>
<tr role="row">
<td class="confluenceTd">&nbsp;
<span style="color: rgb(33,33,33);">X-API-Key</span>&nbsp;
</td>
<td class="confluenceTd">&nbsp;
<span style="color: rgb(33,33,33);">7c6a7c98-20e1-4915-ba0c-985b6724560b</span>&nbsp;
</td>
</tr>
</tbody>
</table>
</div>
<p>The value of the X-API-Key is something configured, it is just an example</p>
<p>The
<span style="color: rgb(33,33,33);">Content-Type value,</span>can be one of the following list:
</p>
<ul>
<li>application/json</li>
<li>application/rdf+xml</li>
<li>application/ld+json</li>
<li>application/n-triples</li>
<li>application/n-quads</li>
<li>application/trig</li>
<li>application/trix</li>
<li>text/turtle</li>
<li>text/n3</li>
</ul>
</td>
<td class="confluenceTd">a file content in rdf+xml format (D-CAT)</td>
<td class="confluenceTd">&nbsp;
<span style="color: rgb(33,33,33);">
<a href="http://healthdata-local.com/hub/api/repocatalogues/ecdc" class="external-link" rel="nofollow">http://healthdata-local.com/hub/api/repo/catalogues/ecdc</a>
</span>&nbsp;
</td>
<td class="confluenceTd">Just the status&nbsp;
<span class="response-meta-status-code" style="color: rgb(0,127,49);">201</span>
<span class="response-meta-status-code-desc" style="color: rgb(0,127,49);">Created</span>&nbsp;
</td>
</tr>
<tr role="row">
<td class="confluenceTd">Get the ecdc catalogue&nbsp;</td>
<td class="confluenceTd">GET</td>
<td class="confluenceTd">
<div class="table-wrap">
<table data-mce-resize="false" class="confluenceTable tablesorter tablesorter-default" role="grid" resolved="">
<colgroup>
<col>
<col></colgroup>
<thead>
<tr role="row" class="tablesorter-headerRow">
<th class="confluenceTh tablesorter-header sortableHeader tablesorter-headerUnSorted" data-column="0" tabindex="0" scope="col" role="columnheader" aria-disabled="false" unselectable="on" aria-sort="none" aria-label="key: No sort applied, activate to apply an ascending sort" style="user-select: none;">
<div class="tablesorter-header-inner">key</div>
</th>
<th class="confluenceTh tablesorter-header sortableHeader tablesorter-headerUnSorted" data-column="1" tabindex="0" scope="col" role="columnheader" aria-disabled="false" unselectable="on" aria-sort="none" aria-label="value: No sort applied, activate to apply an ascending sort" style="user-select: none;">
<div class="tablesorter-header-inner">value</div>
</th>
</tr>
</thead>
<tbody aria-live="polite" aria-relevant="all">
<tr role="row">
<td class="confluenceTd">
<span style="color: rgb(33,33,33);">Content-Type</span>&nbsp;
</td>
<td class="confluenceTd">&nbsp;
<span style="color: rgb(33,33,33);">application/rdf+xml</span>&nbsp;
</td>
</tr>
</tbody>
</table>
</div>
</td>
<td class="confluenceTd">Nothing to set&nbsp;</td>
<td class="confluenceTd">
<span style="color: rgb(33,33,33);">
<a href="http://healthdata-local.com/hub/api/repocatalogues/ecdc" class="external-link" rel="nofollow">http://healthdata-local.com/hub/api/repo/catalogues/ecdc</a>
</span>&nbsp;
</td>
<td class="confluenceTd">The content, it should be in&nbsp;
<br>&nbsp;
<span style="color: rgb(33,33,33);">application/rdf+xml, but it's appearing as
<br>JsonId.&nbsp;</span>
</td>
</tr>
<tr role="row">
<td class="confluenceTd">Delete the ecdc catalogue</td>
<td class="confluenceTd">Delete</td>
<td class="confluenceTd">
<div class="table-wrap">
<table data-mce-resize="false" class="confluenceTable tablesorter tablesorter-default" role="grid" resolved="">
<colgroup>
<col>
<col></colgroup>
<thead>
<tr role="row" class="tablesorter-headerRow">
<th class="confluenceTh tablesorter-header sortableHeader tablesorter-headerUnSorted" data-column="0" tabindex="0" scope="col" role="columnheader" aria-disabled="false" unselectable="on" aria-sort="none" aria-label="key: No sort applied, activate to apply an ascending sort" style="user-select: none;">
<div class="tablesorter-header-inner">key</div>
</th>
<th class="confluenceTh tablesorter-header sortableHeader tablesorter-headerUnSorted" data-column="1" tabindex="0" scope="col" role="columnheader" aria-disabled="false" unselectable="on" aria-sort="none" aria-label="value: No sort applied, activate to apply an ascending sort" style="user-select: none;">
<div class="tablesorter-header-inner">value</div>
</th>
</tr>
</thead>
<tbody aria-live="polite" aria-relevant="all">
<tr role="row">
<td class="confluenceTd">&nbsp;
<span style="color: rgb(33,33,33);">Content-Type</span>&nbsp;
</td>
<td class="confluenceTd">&nbsp;
<span style="color: rgb(33,33,33);">application/rdf+xml</span>&nbsp;
</td>
</tr>
<tr role="row">
<td class="confluenceTd">&nbsp;
<span style="color: rgb(33,33,33);">X-API-Key</span>&nbsp;
</td>
<td class="confluenceTd">&nbsp;
<span style="color: rgb(33,33,33);">7c6a7c98-20e1-4915-ba0c-985b6724560b</span>&nbsp;
</td>
</tr>
</tbody>
</table>
</div>
</td>
<td class="confluenceTd">Nothing to set</td>
<td class="confluenceTd">
<span style="color: rgb(33,33,33);">&nbsp;
<a href="http://healthdata-local.com/hub/api/repocatalogues/ecdc" class="external-link" rel="nofollow">http://healthdata-local.com/hub/api/repo</a>
<a href="http://localhost:9080/catalogues/ecdc" class="external-link" rel="nofollow">/catalogues/ecdc</a>&nbsp;
</span>
</td>
<td class="confluenceTd">Just a status&nbsp;
<span class="response-meta-status-code" style="color: rgb(0,127,49);">204 -</span>
<span class="response-meta-status-code-desc" style="color: rgb(0,127,49);">No Content</span>&nbsp;
</td>
</tr>
<tr role="row">
<td class="confluenceTd">Create a dataset with a specific id</td>
<td class="confluenceTd">PUT</td>
<td class="confluenceTd">
<p>Content-Type and X-API-Key, same as previous</p>
</td>
<td class="confluenceTd">a file content in rdf+xml format (D-CAT)</td>
<td class="confluenceTd">
<span style="color: rgb(33,33,33);">&nbsp;
<a href="http://healthdata-local.com/hub/api/repo/catalogues/ecdc/datasets/origin?originalId=tetanus_data" class="external-link" rel="nofollow">http://healthdata-local.com/hub/api/repo/catalogues/ecdc/datasets/origin?originalId=tetanus_data</a>&nbsp;
</span>
</td>
<td class="confluenceTd">Just the status&nbsp;
<span class="response-meta-status-code" style="color: rgb(0,127,49);">201</span>
<span class="response-meta-status-code-desc" style="color: rgb(0,127,49);">Created</span>&nbsp;
</td>
</tr>
<tr role="row">
<td class="confluenceTd">Get a specific dataset, from a specific catalogue</td>
<td class="confluenceTd">GET</td>
<td class="confluenceTd">
<p>Content-type as previous</p>
</td>
<td class="confluenceTd">Nothing to set&nbsp;</td>
<td class="confluenceTd">
<span style="color: rgb(33,33,33);">&nbsp;&nbsp;
<a href="http://healthdata-local.com/hub/api/repo/catalogues/ecdc/datasets/origin?originalId=tetanus_data" class="external-link" rel="nofollow">http://healthdata-local.com/hub/api/repo/catalogues/ecdc/datasets/origin?originalId=tetanus_data</a>&nbsp;
</span>
</td>
<td style="text-align: left;" class="confluenceTd">The content, it should be in&nbsp;
<br>&nbsp;
<span style="color: rgb(33,33,33);">application/rdf+xml, but it's appearing as
<br>JsonId.&nbsp;</span>
</td>
</tr>
<tr role="row">
<td class="confluenceTd">Delete a specific dataset, from a specific catalogue.</td>
<td class="confluenceTd">DELETE</td>
<td class="confluenceTd">
<p>Content-Type and X-API-Key, same as previous</p>
</td>
<td class="confluenceTd">Nothing to set&nbsp;</td>
<td class="confluenceTd">
<span style="color: rgb(33,33,33);">
<a href="http://healthdata-local.com/hub/api/repo/catalogues/ecdc/datasets/origin?originalId=tetanus_data" class="external-link" rel="nofollow">http://healthdata-local.com/hub/api/repo/catalogues/ecdc/datasets/origin?originalId=tetanus_data</a>&nbsp;
</span>
</td>
<td style="text-align: left;" class="confluenceTd">Just a status&nbsp;
<span class="response-meta-status-code" style="color: rgb(0,127,49);">204 -</span>
<span class="response-meta-status-code-desc" style="color: rgb(0,127,49);">No Content</span>&nbsp;
</td>
</tr>
</tbody>
</table>

## License

[Apache 2.0](http://www.apache.org/licenses/LICENSE-2.0)
